#!/usr/bin/env python3
""" Liaison provides bootstrapping functions
    used when setting up cluster nodes """

import base64
import json
import os
import requests
from flask import Flask, request, jsonify

liaison = Flask(__name__)


@liaison.route('/hostcert')
def hostcert():
    """Returns host certificate for requesting IP"""
    client_ip = request.remote_addr
    cert_file = f'/etc/pki/bootstrapper-vault-cluster/{client_ip}.pem'
    key_file = f'/etc/pki/bootstrapper-vault-cluster/{client_ip}.key'
    if os.path.exists(cert_file) and os.path.exists(key_file):
        with open(cert_file, 'rb') as file:
            cert = file.read()
        encoded_cert = base64.b64encode(cert).decode('utf-8')
        with open(key_file, 'rb') as file:
            key = file.read()
        encoded_key = base64.b64encode(key).decode('utf-8')
        return_value = {'cert': encoded_cert, 'key': encoded_key}
    else:
        return_value = {
            'error': f'certificate for {client_ip} is not available!'
        }

    return jsonify(return_value)


@liaison.route('/ca_chain')
def ca_chain():
    """Returns intermediate the_chain"""
    root_cert_file = '/etc/pki/aditronic-self-signed/intermediate.cert.pem'
    if os.path.exists(root_cert_file):
        the_chain = ['']
        cert_count = 0
        with open(root_cert_file, 'rb') as file:
            for line in file:
                line = line.decode('utf-8')
                if 'END' in line:
                    the_chain[cert_count] += line
                    cert_count = cert_count + 1
                    the_chain.append('')
                else:
                    the_chain[cert_count] += line
        the_chain.pop()
        with open('/tmp/out', 'w', encoding='utf-8') as file:
            print(the_chain, file=file)
        return_value = {'ca_chain': []}
        for cert in the_chain:
            link = base64.b64encode(cert.encode('utf-8')).decode('utf-8')
            return_value['ca_chain'].append(link)
    else:
        return_value = {'error': 'root certificate is not available!'}

    return jsonify(return_value)


@liaison.route('/approle')
def approle():
    """Returnes the approle id"""
    client_ip = request.remote_addr
    approle_file = f'/var/spool/bootstrapper/vault_cluster_{client_ip}.role_id'
    if os.path.exists(approle_file):
        with open(approle_file, 'rb') as file:
            approle_content = file.read()
        approle_data = json.loads(approle_content)
        approle_id = base64.b64encode(
            approle_data['role_id'].encode('utf-8')
        ).decode('utf-8')
        return_value = {'role_id': approle_id}
    else:
        return_value = {'error': 'approle id is not available!'}

    return jsonify(return_value)


@liaison.route('/approle/secret_id')
def secret_id():
    """Returns a wrapped secret id"""
    client_ip = request.remote_addr
    vault_token = os.environ.get('VAULT_TOKEN')
    base64_param = request.args.get('base64')
    approle_file = f'/var/spool/bootstrapper/vault_cluster_{client_ip}.role_id'
    if os.path.exists(approle_file):
        with open(approle_file, 'rb') as file:
            approle_content = file.read()
        approle_data = json.loads(approle_content)
        url = (
            f'http://127.0.0.1:8200/v1/{approle_data["id"]}/secret-id'
        )
        data = {'metadata': '{ "created_by": "liaison" }'}
        headers = {
            'Authorization': f'Bearer {vault_token}',
            'X-Vault-Wrap-TTL': '1h',
        }
        response = requests.post(url, json=data, headers=headers, timeout=10)
        with open(
            f'/tmp/secret_id-{client_ip}', 'w', encoding='utf-8'
        ) as file:
            if response.status_code == 200:
                print(json.dumps(response.json()), file=file)
                wrap_token = response.json()['wrap_info']['token']
                if base64_param and base64_param.lower() in ('true', '1', 'yes', 'on'):
                    wrap_token = base64.b64encode(
                        wrap_token.encode('utf-8')
                    ).decode('utf-8')
                return_value = {'token': wrap_token}
            else:
                print(f'Error: {response}', file=file)
                print(response.json(), file=file)
                return_value = {'error': response.status_code}

    return jsonify(return_value)


if __name__ == '__main__':
    liaison.run(host='127.0.0.1', port=5000, debug=True)
